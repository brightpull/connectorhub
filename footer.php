<?php $partnership = get_field('partnership', 2); ?>
<div class="bg-white py-12">
	<div class="container">
		<div class="flex items-center">
			<img class="mr-6 w-12 lg:w-24" src="<?php echo get_template_directory_uri(); ?>/svg/onedoorlogo.svg" alt="one door logo">
			<img class="mr-8 lg:mr-12 w-12 lg:w-24" src="<?php echo get_template_directory_uri(); ?>/svg/flourishlogo.svg" alt="flourish logo">
			<p class="text-sm lg:text-xl mb-0 font-bold">
				<?php echo $partnership['text']; ?>
			</p>
		</div>
	</div>
</div>

<?php $newsletter = get_field('newsletter', 2); ?>
<div class="bg-blue py-16 text-white">
	<div class="container lg:flex items-center justify-between text-center lg:text-left">
		<img class="inline-block mb-6 lg:mb-0" width="48" src="<?php echo get_template_directory_uri(); ?>/svg/paperplanewhite.svg" alt="mail icon">
		<p class="text-2xl lg:mb-0 font-bold mx-8">
			<?php echo $newsletter['text']; ?>
		</p>
		<a class="c-button--green w-64 lg:w-48 mx-auto lg:w-auto lg:mr-8" href="#" data-js="toggle-newsletter">
			<span>Newsletter Sign Up</span>
		</a>
	</div>
</div>

<div class="bg-navy py-12 text-white">
    <div class="container lg:flex items-center lg:justify-between">
        <div class="lg:flex items-center justify-center lg:mb-0 text-center lg:text-left">
            <a href="https://www.swsphn.com.au" target="_blank">
                <img class="inline-block lg:mr-10 mb-6 lg:mb-0" src="<?php echo get_template_directory_uri(); ?>/svg/phnlogo.svg" alt="phn logo">
            </a>
            <p class="text-sm font-bold mb-0 w-3/4 lg:w-auto mx-auto">This service is supported with funding from the <br class="hidden lg:inline-block">Australian Government through the PHN Program</p>
        </div>
        <!-- <div class="text-center lg:text-right">
            <p class="text-sm">Call the SWSPHN Mental Health Central Intake Line</p>
            <p class="font-bold text-xl mb-0 flex items-center justify-center lg:justify-end">
                <a href="tel:1300797746">
                    <img class="bg-orange rounded-full w-10 h-10 p-2 mr-4" width="8" src="<?php echo get_template_directory_uri(); ?>/svg/phone.svg" alt="phone icon">
                </a>
                <span>
                    1300 797 746<br>
                    1300 SWS PHN
                </span>
            </p>
        </div> -->
    </div>
</div>

<div class="bg-navyDark py-3 text-white">
    <div class="flex items-center justify-between container text-right">
        <p class="mb-0 text-xs">Website &copy; Connector Hub Program</p>
        <p class="mb-0 text-xs">website by <a class="hover:text-green" href="https://brightagency.com.au" target="_blank">bright</a></p>
    </div>
</div>

<?php wp_footer(); ?>

</body>

</html>
