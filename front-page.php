<?php get_header(); ?>

<div class="newsletter">

</div>

<?php $hero = get_field('hero', 2); ?>
<div class="hero pt-32 pb-24 text-white bg-cover bg-center" style="background-image: url('<?php echo $hero['background']; ?>');">
	<div class="container">
		<p class="font-bold mb-5 text-lg">
			<?php echo $hero['small_text']; ?>
		</p>
		<h1 class="font-bold text-5xl lg:text-6xl leading-none mb-10">
			<?php echo $hero['large_text']; ?>
		</h1>
		<div class="flex items-center mb-0">
			<a class="c-button--green mr-4" href="<?php echo site_url(); ?>#what">
				What we do
			</a>
			<a class="c-button--orange" href="<?php echo site_url(); ?>#contact">
				Get in touch
			</a>
		</div>
	</div>
</div>

<?php $introduction = get_field('introduction', 2); ?>
<div class="bg-blue py-16 text-white">
	<div class="container lg:flex items-center text-center lg:text-left">
		<img class="inline-block lg:mr-12 mb-6 lg:mb-0 w-16 lg:w-24" src="<?php echo get_template_directory_uri(); ?>/svg/peopleiconwhite.svg" alt="users icon">
		<p class="mb-0 text-2xl lg:text-4xl leading-snug font-bold">
			<?php echo $introduction['text']; ?>
		</p>
	</div>
</div>

<?php $what = get_field('what_is_connector_hub', 2); ?>
<div class="bg-white py-16" id="what">
	<div class="container grid-2">
		<div>
	        <h2><?php echo $what['heading']; ?></h2>
	        <?php echo $what['text']; ?>
			<a class="c-button--orange inline-block mt-2" href="<?php echo get_permalink(10); ?>">
				Refer online now
			</a>
		</div>
		<div class="lg:mt-0 mt-6">
			<img src="<?php echo $what['image']; ?>" alt="people smiling">
		</div>
	</div>
</div>

<?php $offers = get_field('the_program_offers', 2); ?>
<div class="bg-green py-16">
	<div class="container text-white text-center">
		<h2 class="mb-6"><?php echo $offers['heading']; ?></h2>
		<div class="h-1 bg-navy w-24 mb-2 lg:mb-12 mx-auto"></div>
		<div class="grid-2-lg4">
			<?php foreach( $offers['columns'] as $column ) : ?>
			<div>
				<div class="bg-center bg-no-repeat flex items-center justify-center h-32">
					<img class="w-16 lg:w-24" src="<?php echo get_template_directory_uri(); ?>/svg/<?php echo $column['icon']; ?>.svg" alt="icon">
				</div>
				<h3 class="text-xl lg:text-2xl"><?php echo $column['heading']; ?></h3>
				<p class="mb-0 text-sm lg:text-base"><?php echo $column['text']; ?></p>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>

<?php $who = get_field('who_can_access_connector_hub', 2); ?>
<div class="bg-white py-16">
	<div class="container grid-2">
		<div>
			<img src="<?php echo $who['image']; ?>" alt="people smiling">
		</div>
		<div>
	        <h2><?php echo $who['heading']; ?></h2>
	        <?php echo $who['text']; ?>
			<a class="c-button--orange inline-block mt-2" href="<?php echo get_permalink(10); ?>">
				Refer online now
			</a>
		</div>
	</div>
</div>

<?php $activities = get_field('monthly_activities', 2); ?>
<div class="bg-blue py-16" id="activities">
	<div class="container">
		<div class="container text-white text-center">
			<h2 class="mb-6"><?php echo $activities['heading']; ?></h2>
			<div class="h-1 bg-white w-24 mb-12 mx-auto"></div>
			<div class="block lg:mx-auto lg:w-3/4">
				<?php echo $activities['text']; ?>
			</div>
			<div class="grid-2 lg:w-4/5 lg:mx-auto lg:px-6 mt-16">
				<?php foreach($activities['columns'] as $column) : ?>
					<div>
						<h3><?php echo $column['heading']; ?></h3>
						<p><?php echo $column['text']; ?></p>
						<a class="c-button--green inline-block mt-2 mb-6" href="<?php echo $column['button_link']; ?>" target="_blank">
							View Activities
						</a>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>

<?php $contact = get_field('contact', 2); ?>
<div class="bg-white pt-16 pb-0" id="contact">
	<div class="container">
		<div class="container text-center">
			<h2 class="mb-6"><?php echo $contact['heading']; ?></h2>
			<?php echo $contact['text']; ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
