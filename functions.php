<?php

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

get_template_part( 'includes/cleanup' );

// Add theme support
add_theme_support( 'post-thumbnails' );
add_theme_support( 'title-tag' );

// Enqueue scripts
function bedrock_load_assets()
{
    // jQuery
    if ( !is_admin() ) { wp_deregister_script( 'jquery' ); }
    wp_enqueue_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js', array(), '3.4.1', false );

    // CSS
    wp_enqueue_style( 'aos', 'https://unpkg.com/aos@next/dist/aos.css' );
    wp_enqueue_style( 'app', get_template_directory_uri() . '/style.css', array(), '1.0' );

    // JavaScript
    wp_enqueue_script( 'aos', 'https://unpkg.com/aos@next/dist/aos.js', array(), '3.0.0', true );
    wp_enqueue_script( 'app', get_template_directory_uri() . '/js/app-min.js', array(), null, true );
}
add_action( 'wp_enqueue_scripts', 'bedrock_load_assets' );
