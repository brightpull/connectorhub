<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">

    <meta name="author" content="Bright Agency">

    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">

    <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="bg-navy py-2 text-white">
    <!-- <div class="lg:flex items-center text-center lg:justify-end container lg:text-right">
        <p class="mb-1 lg:mb-0 text-sm">Call the SWSPHN Mental Health Central Intake Line</p>
        <a href="tel:1300797746">
            <img class="hidden lg:block bg-orange rounded-full w-8 h-8 p-2 mx-4" width="8" src="<?php echo get_template_directory_uri(); ?>/svg/phone.svg" alt="phone icon" style="margin-top: -5px">
        </a>
        <p class="font-bold mb-0">1300 797 746 (1300 SWS PHN)</p>
    </div> -->
</div>

<nav class="header bg-white py-8">
    <div class="container">
        <div class="header__logo">
            <a href="<?php echo site_url(); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/svg/connectorhublogo.svg" alt="connector hub logo">
            </a>
        </div>
        <ul class="hidden lg:flex items-center justify-end uppercase mb-0 font-bold">
            <li>
                <a class="text-navy" href="<?php echo site_url(); ?>">
                    <img class="ml-3" width="16" src="<?php echo get_template_directory_uri(); ?>/svg/house.svg" alt="house icon" style="margin-top: -2px">
                </a>
            </li>
            <li class="ml-8">
                <a class="text-navy hover:text-green" href="<?php echo site_url(); ?>#what">
                    What is Connector Hub?
                </a>
            </li>
            <li class="ml-8">
                <a class="text-navy hover:text-green" href="<?php echo site_url(); ?>#activities">
                    Monthly Activities
                </a>
            </li>
            <li class="ml-8">
                <a class="text-navy hover:text-green" href="<?php echo site_url(); ?>#contact">
                    Get In Touch
                </a>
            </li>
            <li class="ml-8">
                <a class="c-button--green" href="<?php echo get_permalink(10); ?>">
                    <span>Refer Online</span>
                    <img class="ml-3" width="20" src="<?php echo get_template_directory_uri(); ?>/svg/peopleiconwhite.svg" alt="users icon" style="">
                </a>
            </li>
        </ul>
        <ul class="flex justify-end items-center lg:hidden mb-0">
            <li>
                <a class="text-navy" href="#" data-js="toggle-menu">
                    <img width="30" src="<?php echo get_template_directory_uri(); ?>/svg/menu.svg" alt="menu icon">
                </a>
            </li>
        </ul>
    </div>
</nav>

<div class="mobile-menu">

    <ul class="font-bold uppercase">
        <li><a href="<?php echo site_url(); ?>">Home</a></li>
        <li><a href="<?php echo site_url(); ?>#what">What is Connector Hub?</a></li>
        <li><a href="<?php echo site_url(); ?>#activities">Monthly Activities</a></li>
        <li><a class="bg-green text-white hover:bg-green-600 hover:text-white" href="<?php echo get_permalink(10); ?>">Refer Online</a></li>
        <li><a href="<?php echo site_url(); ?>#contact">Get In Touch</a></li>
    </ul>

</div>

<div class="newsletter">

    <div class="newsletter__bg">

        <div class="newsletter__box">

            <img class="newsletter__close" data-js="toggle-newsletter" width="18" src="<?php echo get_template_directory_uri(); ?>/svg/close.svg" alt="close">

            <h4 class="mb-4">Newsletter Signup</h4>

            <div class="bg-gray-300 w-24 mb-6" style="height: 2px;"></div>

            <?php echo do_shortcode('[gravityform id="2" title="false" description="false" ajax="true"]'); ?>

        </div>

    </div>

</div>
