// Animate on scroll
AOS.init();

// Menu
jQuery('[data-js="toggle-menu"]').click(function(event) {
    event.preventDefault();
    jQuery('.mobile-menu').slideToggle();
});

// Newsletter
jQuery('[data-js="toggle-newsletter"]').click(function(event) {
    event.preventDefault();
    jQuery('.newsletter').fadeToggle();
});
