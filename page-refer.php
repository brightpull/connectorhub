<?php

/* Template Name: Refer Online */

get_header(); ?>

<?php $hero = get_field('hero', 2); ?>
<div class="hero pt-32 pb-24 text-white bg-cover bg-center" style="background-image: url('<?php echo $hero['background']; ?>');">
	<div class="container">
		<h1 class="font-bold text-5xl lg:text-6xl leading-none">
			Refer Online
		</h1>
	</div>
</div>

<div class="bg-white py-16 pb-0" id="what">
	<div class="container">
		<?php if ( have_posts() ) : ?>
		    <?php while ( have_posts() ) : the_post(); ?>
		        <?php the_content(); ?>
		    <?php endwhile; ?>
		<?php endif; ?>
	</div>
</div>

<?php get_footer(); ?>
